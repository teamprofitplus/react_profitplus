import React from 'react';
import {
  Grid, Button, FormControlLabel, Switch,
  Dialog, DialogActions, DialogContent, DialogTitle, InputLabel, Input, FormControl
} from '@material-ui/core';
import Select from 'react-select';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import PageTitle from '../components/PageTitle/PageTitle';
import {
  PlayArrow as PlayIcon,
  Stop as StopIcon,
} from '@material-ui/icons';


const axios = require('axios');


const columns = [
  {
    name: "userInputId",
    label: "ID",
    options: {
      filter: false,
      sort: true,
    }
  },
  {
    name: "strategy.strategyName",
    label: "Strategy Name",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "strategyCapital",
    label: "Capital Amount(USD $)",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "accountBalance",
    label: "Strategy Balance(USD $)",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "stockSymbol",
    label: "Stock Symbol",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "shortPeriod",
    label: "Short Average",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "longPeriod",
    label: "Long Average",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "multiples",
    label: "Standard Deviation",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "userInputDate",
    label: "Activation Timestamp",
    options: {
      filter: false,
      sort: true,
    }
  },
  {
    name: "accountBalance",
    label: "Current Profit",
    options: {
      filter: false,
      sort: true,
    }
  },
  {
    name: "activate",
    label: "Activation",
    options: {
      filter: false,
      sort: true,
      customBodyRender: (value, tableMeta, updateValue) => {
        return (
          <FormControlLabel
            label={value ? "Activated" : "Deactivated"}
            value={value ? true : false}
            control={
              <Switch
                color="primary"
                checked={value}
                value={value ? "Activated" : "Deactivated"}
              />
            }
            onChange={event => {
              updateValue(event.target.value === "Activated" ? false : true);
            }}
          />
        );
      }
    }
  }
];

const shortAvgOptions = [
  { value: 5, label: '5' },
  { value: 10, label: '10' },
];

const longAvgOptions = [
  { value: 15, label: '15' },
  { value: 20, label: '20' },
];
const multiSDOptions = [
  { value: 1.50, label: '1.50' },
  { value: 1.75, label: '1.75' },
  { value: 2.00, label: '2.00' },
];

const getMuiTheme = () => createMuiTheme({
  overrides: {
    MUIDataTableSelectCell: {
      checkboxRoot: {
        display: 'none'
      }
    }
  }
});

let intervalId;
class HistoryTable extends React.Component {

  state = {
    strategyOpen: false,
    strategy: '',
    stock: '',
    captital: '',
    stockOptions: [],
    strategyType: '',
    strategyHistory: [],
    longAvg: '',
    shortAvg: '',
    multiSD: '',
    strategyOptions: '',
    userInputId:'',
  };

  async componentDidMount() {
    try {
      let response = await axios.get(`http://feed2.conygre.com/API/StockFeed/GetSymbolList`);

      let stockOptions = response.data.map(function (options) {
        return { value: options.Symbol, label: options.CompanyName };
      })
      this.getStrategyHistory();
      this.keepPullingData();
      this.setState({ stockOptions: stockOptions });
    } catch (error) {
      console.log(error)
    }

  }

  componentWillUnmount() {
    window.clearInterval(intervalId);
  }

  async keepPullingData() {
    intervalId = window.setInterval(() => {
      try {
        this.getStrategyHistory();
      } catch{
        console.log("error");
      }

    }, 1000)
  }

  async getStrategyHistory() {
    try {
      const nf = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
      let response = await axios.get(`${process.env.REACT_APP_API}/api/strategyhistory`);
      for (var i = 0; i < response.data.length; i++) {
        response.data[i].accountBalance = nf.format(response.data[i].accountBalance);
        response.data[i].strategyCapital = nf.format(response.data[i].strategyCapital);
        if (response.data[i].shortPeriod === 0) {
          response.data[i].shortPeriod = '-'
          response.data[i].longPeriod = '-'
        }
        else if (response.data[i].multiples === 0) {
          response.data[i].multiples = '-'
        }

      }
      this.setState({ strategyHistory: response.data });
    } catch (error) {
      console.log(error)
    }
  }
  handleCaptitalChange = (event) => {
    this.setState({ captital: event.target.value });
  }

  handleCloseStrategyDialog = () => {
    this.setState({ strategyOpen: false });
  }

  handleCaptitalChange = (event) => {
    this.setState({ captital: event.target.value });
  }

  handleShortAvgChange = (shortAvg) => {
    this.setState({ shortAvg });
  }

  handleLongAvgChange = (longAvg) => {
    this.setState({ longAvg });
  }
  handlemMultiSDChange = (multiSD) => {
    this.setState({ multiSD });
  }

  handleOnCellClick = (rowMeta, cellMeta) => {
    const { history } = this.props;
    if (cellMeta.colIndex === 10) {
      axios.put(`${process.env.REACT_APP_API}/api/updatestrategystatus/${this.state.strategyHistory[cellMeta.dataIndex].userInputId}`, {
      }).then((response) => {
        console.log(response);
      }).catch((err) => {
        console.log(err);
      })
    } else {
      for (var i = 0; i < this.state.stockOptions.length; i++) {
        if (this.state.stockOptions[i].value === this.state.strategyHistory[cellMeta.rowIndex].stockSymbol) {
          this.setState({ stock: this.state.stockOptions[i] })
          break;
        }
      }

      if (this.state.strategyHistory[cellMeta.rowIndex].multiples > 0) {
        for (var i = 0; i < multiSDOptions.length; i++) {
          if(this.state.strategyHistory[cellMeta.rowIndex].multiples === multiSDOptions[i].value)
          {
            this.setState({multiSD: multiSDOptions[i]});
          }
        }
      } else {
        for (var i = 0; i < longAvgOptions.length; i++) {
          if(this.state.strategyHistory[cellMeta.rowIndex].longPeriod === longAvgOptions[i].value)
          {
            this.setState({
              longAvg: longAvgOptions[i]
            });
          }
          if(this.state.strategyHistory[cellMeta.rowIndex].shortPeriod === shortAvgOptions[i].value)
          {
            this.setState({
              shortAvg: shortAvgOptions[i]
            });
          }
        }
      }

      let stringCapital = this.state.strategyHistory[cellMeta.rowIndex].strategyCapital
      stringCapital = stringCapital.replace('$', '').replace(',', '');
      let convertCapital = parseFloat(stringCapital);

      this.setState({
        captital: convertCapital,
        strategyType: this.state.strategyHistory[cellMeta.rowIndex].strategy.strategyId,
        strategy: { value: this.state.strategyHistory[cellMeta.rowIndex].strategy.strategyId, label: this.state.strategyHistory[cellMeta.rowIndex].strategy.strategyName },
        userInputId: this.state.strategyHistory[cellMeta.dataIndex].userInputId,
        strategyOpen: true,
      })
    }

  }

  handlerStragegySubmit = () => {
    const { strategyType, captital, stock, longAvg, shortAvg, multiSD } = this.state;

    try {
      axios.put(`${process.env.REACT_APP_API}/api/updatestrategy/${this.state.userInputId}`, {
        strategyId: strategyType,
        strategyCapital: captital,
        stockSymbol: stock.value,
        longPeriod: longAvg.value,
        shortPeriod: shortAvg.value,
        multiples: multiSD.value,
      }).then(function (response) {
        console.log(response);
      }).catch(function (error) {
        console.log(error);
      });
    } catch (error) {
      console.log(error)
    }
    // console.log(this.state.strategy.value)
    // console.log(this.state.stock.value)
    // console.log(this.state.captital)
    // console.log(shortAvg.value);

    this.setState({
      strategyOpen: false,
      strategy: '',
      stock: '',
      captital: '',
      shortAvg: '',
      longAvg: '',
      multiSD: '',
    });
  }

  render() {
    const { strategyHistory } = this.state
    let extraInputs;
    if (this.state.strategyType === 1) {
      extraInputs = [
        <FormControl fullWidth style={{ marginTop: 20, marginBottom: 20, }}>
          <Select fullWidth
            options={shortAvgOptions}
            placeholder={"Please choose a Short Average"}
            onChange={this.handleShortAvgChange} value={this.state.shortAvg}>
          </Select>
        </FormControl>,
        <FormControl fullWidth >
          <Select fullWidth
            options={longAvgOptions}
            placeholder={"Please choose a Long Average"}
            onChange={this.handleLongAvgChange} value={this.state.longAvg}>
          </Select>
        </FormControl>
      ]
    } else if (this.state.strategyType === 2) {
      extraInputs = [
        <FormControl fullWidth style={{ marginTop: 20, marginBottom: 20, }}>
          <Select fullWidth
            options={multiSDOptions}
            placeholder={"Please choose a Multiples of Standard Deviation"}
            onChange={this.handlemMultiSDChange} value={this.state.multiSD}>
          </Select>
        </FormControl>

      ]
    }
    return (
      <Grid>

        <PageTitle title="Strategy History" />
        <Grid container spacing={32}>
          <Grid item xs={12}>
            <MuiThemeProvider theme={getMuiTheme()}>
              <MUIDataTable
                // title="Strategy History"
                data={strategyHistory}
                columns={columns}
                options={{
                  filterType: 'checkbox',
                  onCellClick: (colData, cellMeta) => {
                    this.handleOnCellClick(colData, cellMeta);
                  },
                }}
              />
            </MuiThemeProvider>
          </Grid>
          <Dialog
            open={this.state.strategyOpen}
            onClose={this.handleCloseStrategyDialog}
          >
            {/* <form onSubmit={this.handlerStragegySubmit}> */}
            <DialogTitle style={{ cursor: 'move' }} >
              Editing strategy
                        </DialogTitle>
            <DialogContent style={{ height: 500, width: 500 }}>
              <Grid>
                <FormControl fullWidth>
                  <Select isDisabled={true}
                    value={this.state.strategy}>
                  </Select>
                </FormControl>
                <FormControl fullWidth style={{ marginTop: 20, marginBottom: 10, }}>
                  <Select isDisabled={true}
                    value={this.state.stock} >
                  </Select>
                </FormControl>
                <FormControl fullWidth>
                  <InputLabel htmlFor="captital">Initial Investment Amount</InputLabel>
                  <Input id="captital" type="number" value={this.state.captital} onChange={this.handleCaptitalChange} />

                </FormControl>
                {extraInputs}
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleCloseStrategyDialog} color="secondary">
                Cancel
                        </Button>
              <Button onClick={this.handlerStragegySubmit} color="primary">
                Confirm
                        </Button>
            </DialogActions>
          </Dialog>
        </Grid>
      </Grid>
    );
  }
}


export default HistoryTable;