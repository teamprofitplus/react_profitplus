import React, { useState } from 'react';
import {
    Grid, Button,
    Dialog, DialogActions, DialogContent, DialogTitle,  InputLabel, Input, FormControl
} from '@material-ui/core';
import Widget from '../../components/Widget';
import AddIcon from '@material-ui/icons/Add';
import ApexCharts from 'apexcharts'
import ReactApexCharts from "react-apexcharts";

import Select from 'react-select';
import { withTheme } from "@material-ui/core";
import PageTitle from "../../components/PageTitle";

const axios = require('axios');

const shortAvgOptions = [
    { value: 5, label: '5' },
    { value: 10, label: '10' },
];

const longAvgOptions = [
    { value: 15, label: '15' },
    { value: 20, label: '20' },
];
const multiSDOptions = [
    { value: 1.50, label: '1.50' },
    { value: 1.75, label: '1.75' },
    { value: 2.00, label: '2.00' },
];

const strategyOptions = [
    { value: 1, label: 'The Moving Average' },
    { value: 2, label: 'Bollinger Bands' },
    { value: 3, label: 'Price Breakout' },
];

let intervalId;
let XAXISRANGE = 86400000 / 24 / 10
var data = []
async function getStartFeeds(date, symbol, intervals) {
    try {
        let response = await axios.get(`http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/${symbol}?HowManyValues=${intervals}`)
        // .then(function (response) {
        // handle success
        var datetime;
        var dateTimeStamp;
        var i = 0;
        var today = new Date();
        var year = today.getFullYear();
        var month = Number(String(today.getMonth()).padStart(2,'0'));
        var day = Number(String(today.getDate()).padStart(2,'0'));
        while (i < intervals) {
            datetime = response.data[i].tradeTime.split(":");

            dateTimeStamp = new Date(year, month, day, parseInt(datetime[0]) + 8, datetime[1], datetime[2])

            var x = dateTimeStamp.getTime();
            var y = response.data[i].Price

            data.push({
                x, y
            });
            i++;
        }
        // console.log(data);
    } catch (error) {
        console.log(error);
    }
}

function getFeeds(symbol, intervals) {
    axios.get(`http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/${symbol}?HowManyValues=${intervals}`)
        .then(function (response) {
            // handle success
            var lastDate = response.data[0].tradeTime
            var today = new Date();
            var datetime = lastDate.split(":");
            var year = today.getFullYear();
            var month = Number(String(today.getMonth()).padStart(2,'0'));
            var day = Number(String(today.getDate()).padStart(2,'0'));
            var dateTimeStamp = new Date(year, month, day, parseInt(datetime[0]) + 8, datetime[1], datetime[2])

            // data[i].x = response.data.tradeTime
            // data[i].y = response.data.Price

            for (var i = 0; i < data.length - 1000; i++) {
                // IMPORTANT
                // we reset the x and y of the data which is out of drawing area
                // to prevent memory leaks
                data[i].x = 0
                data[i].y = 0
            }

            data.push({
                x: dateTimeStamp.getTime(),
                y: response.data[0].Price
            })

        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
}

class ChartViewReact extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            realTimeStock: { value: 'aapl', label: 'Apple Inc' },
            stockOptions: [],
            strategyOpen: false,
            strategy: '',
            stock: '',
            captital: 10000,
            shortAvg: '',
            longAvg: '',
            multiSD: '',
            options: {
                chart: {
                    id: 'realtime',
                    animations: {
                        enabled: true,
                        easing: 'linear',
                        dynamicAnimation: {
                            speed: 1000
                        }
                    },
                    toolbar: {
                        show: false
                    },
                    zoom: {
                        enabled: false
                    }
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'smooth'
                },

                // title: {
                //     // text: 'Real Time Stock',
                //     align: 'left'
                // },
                markers: {
                    size: 0
                },
                xaxis: {
                    type: 'datetime',
                    range: XAXISRANGE,
                },
                yaxis: {
                    // max: 100
                },
                legend: {
                    show: false
                }
            },
            series: [{
                data: data.slice()
            }],
            error: null,
            errorInfo: null
        }
    };


    async componentDidMount() {
        try {
            let response = await axios.get(`http://feed2.conygre.com/API/StockFeed/GetSymbolList`);

            let stockOptions = response.data.map(function (options) {
                return { value: options.Symbol, label: options.CompanyName };
            })
            data=[]
            getStartFeeds(new Date().getTime(), this.state.realTimeStock.value, 300)
            this.intervals()
            this.setState({ stockOptions: stockOptions });
        } catch (error) {
            console.log(error)
        }
    }
    componentWillUnmount() {
        window.clearInterval(intervalId);
    }

    handleOpenStrategyDialog = () => {
        this.setState({ strategyOpen: true });
    }

    handleCloseStrategyDialog = () => {
        this.setState({ strategyOpen: false });
    }

    handlerStragegySubmit = () => {
        const { history } = this.props;
        const { strategy, captital, stock, longAvg, shortAvg, multiSD } = this.state;

        try {
            axios.post(`${process.env.REACT_APP_API}/api/userInput`, {
                strategyId: strategy.value,
                strategyCapital: captital,
                stockSymbol: stock.value,
                longPeriod: longAvg.value,
                shortPeriod: shortAvg.value,
                multiples: multiSD.value,
            }).then(function (response) {
                console.log(response);
            })
                .catch(function (error) {
                    console.log(error);
                });
            history.push({
                pathname: "/strategyhistory",
            });
        } catch (error) {
            console.log(error)
        }
        // console.log(this.state.strategy.value)
        // console.log(this.state.stock.value)
        // console.log(this.state.captital)
        // console.log(shortAvg.value);

        this.setState({
            strategyOpen: false,
            strategy: '',
            stock: '',
            captital: '',
            shortAvg: '',
            longAvg: '',
            multiSD: '',
        });
    }

    handleStrategeChange = (strategy) => {
        this.setState({ strategy });
    }

    handleStockChange = (stock) => {
        this.setState({ stock });
    }

    handleCaptitalChange = (event) => {
        this.setState({ captital: event.target.value });
    }

    handleShortAvgChange = (shortAvg) => {
        this.setState({ shortAvg });
    }

    handleLongAvgChange = (longAvg) => {
        this.setState({ longAvg });
    }
    handlemMultiSDChange = (multiSD) => {
        this.setState({ multiSD });
    }

    handleRealTimeStockChange = (realTimeStock) => {
        this.setState({ realTimeStock });
        data = []
        window.clearInterval(intervalId);
        getStartFeeds(new Date().getTime(), realTimeStock.value, 300)
        this.intervals()
    }

    intervals() {
        intervalId = window.setInterval(() => {
            // getNewSeries(lastDate, {
            //     min: 10,
            //     max: 90
            // })
            try {
                getFeeds(this.state.realTimeStock.value, 1)
                ApexCharts.exec('realtime', 'updateSeries', [{
                    data: data
                }])
            } catch{
                // window.clearInterval();
                console.log("error");
            }

        }, 1000)
    }

    render() {
        const { classes } = this.props;
        let extraInputs;
        if (this.state.strategy.value === 1) {
            extraInputs = [

                <FormControl fullWidth style={{ marginTop: 20, marginBottom: 20, }}>
                    <Select fullWidth 
                        options={shortAvgOptions}
                        placeholder={"Please choose a Short Average"}
                        onChange={this.handleShortAvgChange} value={this.state.shortAvg}>
                    </Select>
                </FormControl>,
                <FormControl fullWidth >
                    <Select fullWidth
                        options={longAvgOptions}
                        placeholder={"Please choose a Long Average"}
                        onChange={this.handleLongAvgChange} value={this.state.longAvg}>
                    </Select>
                </FormControl>

            ]
        } else if (this.state.strategy.value === 2) {
            extraInputs = [
                <FormControl fullWidth style={{ marginTop: 20, marginBottom: 20, }}>
                    <Select fullWidth
                        options={multiSDOptions}
                        placeholder={"Please choose a Multiples of Standard Deviation"}
                        onChange={this.handlemMultiSDChange} value={this.state.multiSD}>
                    </Select>
                </FormControl>

            ]
        }

        return (
            <Grid>
                <PageTitle title="Real Time Stock Charts" />
                <Grid container spacing={32}>
                    <Grid item xs={12} md={12}>
                        <Grid item xs={3} md={3} style={{ marginBottom: 10, }}>
                            <Select
                                options={this.state.stockOptions}
                                placeholder={"Please choose a stock"}
                                onChange={this.handleRealTimeStockChange} value={this.state.realTimeStock} >
                            </Select>
                        </Grid>
                        <Widget title={this.state.realTimeStock.label} upperTitle noBodyPadding disableWidgetMenu>
                            <div id="chart">
                                <ReactApexCharts options={this.state.options} series={this.state.series} type="line" height="400" />
                            </div>
                        </Widget>
                    </Grid>
                    <Grid
                        style={{ paddingTop: 60 }}
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Button variant="contained" color="primary" onClick={this.handleOpenStrategyDialog} >
                            <AddIcon /> Create a New Strategy
                    </Button>
                    </Grid>
                </Grid>
                <Dialog
                    open={this.state.strategyOpen}
                    onClose={this.handleCloseStrategyDialog}
                >
                    {/* <form onSubmit={this.handlerStragegySubmit}> */}
                    <DialogTitle style={{ cursor: 'move' }} >
                        Adding a new strategy
                        </DialogTitle>
                    <DialogContent style={{ height: 500, width: 500 }}>
                        <Grid>
                            <FormControl fullWidth>
                                <Select
                                    options={strategyOptions}
                                    placeholder={"Please choose a strategy"}
                                    onChange={this.handleStrategeChange} value={this.state.strategy}>
                                </Select>
                            </FormControl>
                            <FormControl fullWidth style={{ marginTop: 20, marginBottom: 10, }}>
                                <Select
                                    options={this.state.stockOptions}
                                    placeholder={"Please choose a stock"}
                                    onChange={this.handleStockChange} value={this.state.stock} >
                                </Select>
                            </FormControl>
                            <FormControl fullWidth>
                                <InputLabel htmlFor="captital">Initial Investment Amount</InputLabel>
                                <Input id="captital" type="number" value={this.state.captital} onChange={this.handleCaptitalChange} />

                            </FormControl>
                            {extraInputs}
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseStrategyDialog} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handlerStragegySubmit} color="primary">
                            Confirm
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        );
    }
}

export default ChartViewReact;