import { compose, withState, withHandlers } from "recompose";

import ChartsView from "./ChartsView";

export default compose(
  withState("activeIndex", "setActiveIndexId", 0),
  withState("addOpen", "setAddOpen", 1),
  withHandlers({
    changeActiveIndexId: props => (event, id) => {
      props.setActiveIndexId(id);
    },
    handleAddClose: props => (open) =>{
      props.setAddOpen(open);
    }
  })
  
)(ChartsView);
