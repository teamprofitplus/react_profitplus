import React, { Component } from "react";
import ApexCharts from 'apexcharts'
import ReactApexCharts from "react-apexcharts";
const axios = require('axios');

var lastDate = 0;
var data = []
var TICKINTERVAL = 86400000
let XAXISRANGE = 86400000 / 24 / 10


async function getStartFeeds(date, symbol, intervals) {
    try {
        let response = await axios.get(`http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/${symbol}?HowManyValues=${intervals}`)
        // .then(function (response) {
        // handle success
        var datetime;
        var dateTimeStamp;
        var i = 0;
        var year = 2019;
        var month = 8;
        var day = 25;
        while (i < intervals) {
            datetime = response.data[i].tradeTime.split(":");

            dateTimeStamp = new Date(year, month, day, parseInt(datetime[0])+8, datetime[1], datetime[2])

            var x = dateTimeStamp.getTime();
            var y = response.data[i].Price

            data.push({
                x, y
            });
            i++;
        }
        // console.log(data);
    } catch (error) {
        console.log(error);
    }
    // })
    // .catch(function (error) {
    //     // handle error
    //     console.log(error);
    // })
    // .finally(function () {
    //     // always executed
    // });
}


function getFeeds(symbol, intervals) {
    axios.get(`http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/${symbol}?HowManyValues=${intervals}`)
        .then(function (response) {
            // handle success
            // console.log(response.data[0].tradeTime.getTime())
            lastDate = response.data[0].tradeTime
            // var today = new Date().getTime();
            var today = new Date();
            // var date = new Date().getTime() - 
            var datetime = lastDate.split(":");
            var year = 2019;
            var month = 8;
            var day = 25;
            var dateTimeStamp = new Date(year, month, day, parseInt(datetime[0])+8, datetime[1], datetime[2])

            // data[i].x = response.data.tradeTime
            // data[i].y = response.data.Price

            for (var i = 0; i < data.length - 1000; i++) {
                // IMPORTANT
                // we reset the x and y of the data which is out of drawing area
                // to prevent memory leaks
                data[i].x = 0
                data[i].y = 0
            }

            data.push({
                x: dateTimeStamp.getTime(),
                y: response.data[0].Price
            })

        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
}

function getNewSeries(baseval, yrange) {
    var newDate = baseval + TICKINTERVAL;
    lastDate = newDate

    for (var i = 0; i < data.length - 100; i++) {
        // IMPORTANT
        // we reset the x and y of the data which is out of drawing area
        // to prevent memory leaks
        data[i].x = newDate - XAXISRANGE - TICKINTERVAL
        data[i].y = 0
    }

    data.push({
        x: newDate,
        y: Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min
    })
    // console.log(data);

}

function resetData() {
    // Alternatively, you can also reset the data at certain intervals to prevent creating a huge series 
    data = data.slice(data.length - 10, data.length);
}
let intervalId;
class LineChart extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            options: {
                chart: {
                    id: 'realtime',
                    animations: {
                        enabled: true,
                        easing: 'linear',
                        dynamicAnimation: {
                            speed: 1000
                        }
                    },
                    toolbar: {
                        show: false
                    },
                    zoom: {
                        enabled: false
                    }
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'smooth'
                },

                title: {
                    text: 'Real Time Stock',
                    align: 'left'
                },
                markers: {
                    size: 0
                },
                xaxis: {
                    type: 'datetime',
                    range: XAXISRANGE,
                },
                yaxis: {
                    // max: 100
                },
                legend: {
                    show: false
                }
            },
            series: [{
                data: data.slice()
            }],
            error: null,
            errorInfo: null
        }
    }

    componentDidMount() {
        getStartFeeds(new Date().getTime(), 'aapl', 300)
        this.intervals('aapl')
    }
    componentWillUnmount() {
        window.clearInterval(intervalId);
    }

    hanleChangeStock() {
        this.data = []
        window.clearInterval(intervalId);
        this.intervals('ibm')
    }

    intervals(stockSymbol) {
        intervalId = window.setInterval((stockSymbol) => {
            // getNewSeries(lastDate, {
            //     min: 10,
            //     max: 90
            // })
            try {
                getFeeds(stockSymbol, 1)
                ApexCharts.exec('realtime', 'updateSeries', [{
                    data: data
                }])
            } catch{
                // window.clearInterval();
                console.log("error");
            }

        }, 1000)
    }

    render() {

        return (

            <div id="chart">
                <ReactApexCharts options={this.state.options} series={this.state.series} type="line" height="400" />
            </div>

        );
    }

}

export default LineChart;
